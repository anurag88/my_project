from django.db import models
from datetime import date,datetime
# Create your models here.
class Product(models.Model):
  title       = models.TextField(max_length=120,)
  description = models.TextField(blank=True, null=True)
  price       = models.DecimalField(decimal_places=2, max_digits=10000)
  summary     = models.TextField()
  featured    = models.BooleanField(default=False)