from django import forms

from .models import Product

class ProductForm(forms.ModelForm):
  class Meta:
    model = Product
    fields = [
      'title',
      'description',
      'price',
      'featured'
    ]

class RawProductForm(forms.Form):
  title       = forms.CharField(label='Product Title',widget=forms.TextInput
                                (attrs={
    "placeholder":"Enter Product Name"
  }))
  description = forms.CharField(required=True, 
                                widget=forms.Textarea(
                                  attrs={
                                    "placeholder":"Enter Product Description",
                                    "class":"new-class-name two",
                                    "rows":20,
                                    "cols": 40,
                                    "id":"my-id-for-textarea"
                                  }
                                ))
  price       = forms.DecimalField(initial=199.99)
  featured    = forms.BooleanField(required=False)